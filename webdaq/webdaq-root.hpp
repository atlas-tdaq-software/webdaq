#ifndef WEBDAQ_ROOT_API_H
#define WEBDAQ_ROOT_API_H

#include <string>

class TObject;

namespace webdaq {

    namespace oh {

        // 'obj' can be TH*, TGraph*, TEfficiency*

        bool put(const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 TObject *obj);

        bool put(const std::string& partition,
                 const std::string& server_dot_name,
                 TObject *obj);
    }
}

#endif // WEBDAQ_ROOT_API_H
