#ifndef WEBDAQ_CURL_HPP_
#define WEBDAQ_CURL_HPP_

#include "nlohmann/json_fwd.hpp"
#include <chrono>

#ifndef LIBCURL_VERSION
typedef void CURL;
#endif

namespace webdaq {

    // This header contains the same functions as webdaq.hpp
    // but with an explicit CURL handle as the first arguments.
    // It is indented for clients who do not want to rely on
    // the thead-local global CURL session that is used by
    // default.

    std::string_view
    base_url();

    /// Set a new base URL
    void
    base_url(std::string_view base);

    namespace ipc {
        /// Get a list of partitions
        ///
        /// returns a JSON array of partition names
        bool partitions(CURL *handle, nlohmann::json& result);

        // returns a list of partition names
        bool partitions(CURL *handle, std::vector<std::string>& result);
    }

    namespace is {

        /// Retrieve TDAQ IS object as JSON object.
        ///
        /// If 'meta' is true, return an array
        /// with the first three items being
        /// [ <name>, <type>, <date-time>, ...]
        /// and the last entry the object itsef.
        ///
        /// Otherwise 'result' is just the IS object.
        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& server,
            const std::string& name,
            nlohmann::json& result,
            bool meta = false,
            int tag = -1);

        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& server_dot_name,
            nlohmann::json& result,
            bool meta = false,
            int tag = -1);

        /// Update/create a TDAQ IS object from JSON object.
        ///
        /// Note that 'type' is required as we can't
        /// infer it otherwise.
        bool
        put(CURL *handle,
            const std::string& partition,
            const std::string& server,
            const std::string& name,
            const std::string& type,
            const nlohmann::json& data,
            int tag = -1);

        bool
        put(CURL *handle,
            const std::string& partition,
            const std::string& server_dot_name,
            const std::string& type,
            const nlohmann::json& data,
            int tag = -1);

        /// List objects in IS server.
        ///
        /// Potentially filter by regular expression
        /// or type
        bool
        list(CURL *handle,
             const std::string& partition,
             const std::string& server,
             nlohmann::json& results,
             const std::string& regex = "",
             const std::string& type = ""
             );

        /// Remove an IS object
        bool
        remove(CURL *handle,
               const std::string& partition,
               const std::string& server,
               const std::string& name,
               int tag = -1);

        bool
        remove(CURL *handle,
               const std::string& partition,
               const std::string& server_dot_name,
               int tag = -1);

        /// Get list of tags for an object.
        bool
        tags(CURL *handle,
             const std::string& partition,
             const std::string& server,
             const std::string& name,
             std::vector<std::pair<int, int>>& tags);

        bool
        tags(CURL *handle,
             const std::string& partition,
             const std::string& server_dot_name,
             std::vector<std::pair<int, int>>& tags);
    }

    namespace oh {

        /// Get histogram as string
        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& server,
            const std::string& name,
            std::string& result,
            int tag = -1);

        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& server_dot_name,
            std::string& result,
            int tag = -1);

        /// Put histogram from string
        bool
        put(CURL *handle,
            const std::string& partition,
            const std::string& server,
            const std::string& name,
            const std::string& histo,
            int tag = -1);

        bool
        put(CURL *handle,
            const std::string& partition,
            const std::string& server_dot_name,
            const std::string& histo,
            int tag = -1);

        /// Get histogram as JSON
        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& server,
            const std::string& name,
            nlohmann::json& histo,
            int tag = -1);

        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& server_dot_name,
            nlohmann::json& histo,
            int tag = -1);

        /// Put histogram from JSON
        bool
        put(CURL *handle,
            const std::string& partition,
            const std::string& server,
            const std::string& name,
            const nlohmann::json& histo,
            int tag = -1);

        bool
        put(CURL *handle,
            const std::string& partition,
            const std::string& server_dot_name,
            const nlohmann::json& histo,
            int tag = -1);

        // To remove a histogram call:
        // is::remove(partition, server, name);

    }

    namespace ers {
        /// Send an ERS error message
        ///
        /// Supported keys and types:
        ///
        /// "msg" : "class_name"
        /// "sev" : "severity level"    // LOG,DEBUG,INFO,WARNING,ERROR,FATAL
        /// "time": "%Y-%b-%d %H:%M:%S" // formatted time string
        /// "message": "The free text error message"
        /// "params: {
        ///            { "param1", "value1" },
        ///            { "param2", "value2" },
        ///            ...
        ///          }
        /// "qual" : [ "qual1", "qual2" ,... ]
        /// "app"  : "application_name"
        /// "host" : "host_name"
        /// "file" : "file_name.cxx"
        /// "func" : "function_name()"
        /// "pkg"  : "package_name"
        /// "line" : 32
        /// "user" : "user_name"
        /// "pid"  : 12345

        bool
        send(CURL *handle,
             const std::string& partition,
             const nlohmann::json& issue);
    }

    namespace oks {

        /// Retrieve TDAQ OKS object as JSON object
        bool
        get(CURL *handle,
            const std::string& partition,
            const std::string& class_name,
            const std::string& name,
            nlohmann::json& result);

        /// List TDAQ OKS objects as JSON object
        bool
        list(CURL *handle,
             const std::string& partition,
             const std::string& class_name,
             nlohmann::json& results);
    }

    namespace emon {

        /// Subscribe, success if result not empty
        ///
        /// "sampler_type"       : "dcm"
        /// "sampler_keys"       : [ "key1", "key2", ...]
        /// "sampler_count"      : 10
        ///
        /// "status_word"        : 0
        /// "lvl1_trigger_type"  : 0
        /// "lvl1_trigger_bits"  : [ 1, 10, 245 ]
        /// "lvl1_trigger_logic" : "IGNORE"     // "AND", "OR"
        /// "lvl1_trigger_origin : "AFTER_VETO" // "BEFORE_PRESCALES", "AFTER_PRESCALES"
        ///
        /// "stream_tag"         :  "physics"
        /// "stream_names"       : [ "stream1", "stream2", ...]
        /// "stream_logic"       : "IGNORE"     // "AND", "OR"
        std::string
        subscribe(CURL *handle,
                  const std::string& partition,
                  const nlohmann::json& subscription);

        /// Unsubscribe
        bool
        unsubscribe(CURL *handle,
                    const std::string& partition,
                    const std::string& subscription_id);

        /// Get next event, return false in
        /// case of error or timeout.
        bool
        nextEvent(CURL *handle,
                  const std::string& partition,
                  const std::string& subscription_id,
                  std::unique_ptr<uint32_t[]>& event,
                  size_t& length,
                  const std::chrono::system_clock::duration& timeout = std::chrono::seconds(10));
    }

}

#endif // WEBDAQ_CURL_HPP_
