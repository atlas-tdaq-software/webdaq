# webdaq

Support an alternative API where each function has
an additional `CURL *` argument. This allows the user
complete control

## tdaq-common-10-00-00

The `webdaq` package provides an HTTP based client
API to the basic online TDAQ services like IS, OH, EMON
and OKS. It only depends on curl and nlohmann::json libraries
and can be compiled in a stand-alone mode outside of any
TDAQ specific environment. It is part of tdaq-common to
make it automatically available to offline projects that
don't build against the full TDAQ stack.

Possible use cases are

  * Use in Athena without requiring the full TDAQ stack
  * Use in a stand-alone SoC environment without TDAQ software

### Configuration

The library requires the `TDAQ_WEBDAQ_BASE` environment variable
to be set. It should include the URL to access a running
(webis_server)[https://gitlab.cern.ch/atlas-tdaq-software/webis_server]
instance, e.g. https://atlasop.cern.ch or http://localhost:8080

To publish any kind of information the `webis_server` instance has to
be started with the `--writable` option. Note that this is not the case for
the instance that is accessible from outside Point 1 under
https://atlasop.cern.ch

### API

For the API see the main
(header file)[https://gitlab.cern.ch/atlas-tdaq-software/webdaq/-/blob/main/webdaq/webdaq.hpp].

