
#include "webdaq/webdaq-curl.hpp"
#include "common.hpp"
#include <nlohmann/json.hpp>

#include <algorithm>
#include <curl/curl.h>
#include <cstdlib>
#include <memory>
#include <string>

namespace webdaq {

    namespace detail {

        // internally managed CURL handle, one per thread
        thread_local std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> curl{nullptr, nullptr};

        // Get thread local global handle, create it if necessary
        CURL *
        get_handle()
        {
            if(!curl) {
                curl = std::unique_ptr<CURL, decltype(&curl_easy_cleanup)>(curl_easy_init(), curl_easy_cleanup);
            }

            return curl.get();
        }

        // Global CURL initialization
        struct CurlInit {
            CurlInit()
            {
                curl_global_init(CURL_GLOBAL_ALL);
            }

            ~CurlInit()
            {
                curl_global_cleanup();
            }
        };

        void ensure_initialized()
        {
            static CurlInit curl_init;
        }

        // Callback function for receiving data from CURL into a string
        size_t
        receive_data(void *buffer, size_t size, size_t nmemb, void *userp)
        {
            std::string *data = reinterpret_cast<std::string*>(userp);
            data->append(reinterpret_cast<char *>(buffer), nmemb * size);
            return nmemb * size;
        }

        // Helper struct for binary data transfer
        //
        // 'length' is taken from Content-length: header field
        // 'data' is allocated on first data transfer
        // 'pos' is the current position in the buffer
        //
        // There is no guarantee data comes aligned, so 'pos'
        // and 'length' deal with bytes, not uint32_t
        struct binary_data {
            std::unique_ptr<uint32_t[]> data;
            char                        *pos   = nullptr;
            ssize_t                     length = 0;
        };

        // Receive binary data from CURL
        // This handles only the raw event data use case, where length
        // should be a multiple of sizeof(uint32_t).
        size_t
        receive_binary(void *buffer, size_t size, size_t nmemb, void *userp)
        {
            binary_data *p = static_cast<binary_data*>(userp);

            if(!p->data) {
                // we have not yet allocated buffer space

                if(p->length == 0) {
                    // we didn't receive a Content-length: header
                    return 0;
                }

                // 'length' should be a multiple of 4, make sure we have enough
                // space even if that assumption is not correct
                p->data.reset(new uint32_t[(p->length - 1)/sizeof(uint32_t) + 1]);
                p->pos  = reinterpret_cast<char*>(p->data.get());
            }

            // if we get more data than told in the header we fail
            if(p->pos + nmemb * size - (char*)p->data.get() > p->length) {
                return 0;
            }

            std::copy((char *)buffer, (char *)buffer + nmemb * size, p->pos);
            p->pos += nmemb*size;

            return nmemb * size;
        }

        // Helper to parse header fields for a Content-length: entry
        //
        // This allows us to allocate exactly the amount of buffer space
        // we need once, without resizing it later.
        size_t
        header_callback(char *buffer,
                        size_t size,
                        size_t nitems,
                        void *userdata)
        {
            constexpr size_t len = sizeof("content-length:") - 1;
            if(strncasecmp(buffer, "content-length:", len) == 0) {
                std::string s(buffer + len, nitems - len);
                binary_data* p = static_cast<binary_data*>(userdata);
                p->length = stoi(s);
            }
            return nitems * size;
        }

        // Common setup for all curl_get() routines
        bool
        curl_common(CURL *handle,
                    const std::string &url_path,
                    std::string &response)
        {
            if(!handle) {
                return false;
            }

            std::string base_url{webdaq::base_url()};
            if(base_url.empty()) {
                return false;
            }

            std::string unix_socket;
            if(base_url.find("unix:") == 0) {
                unix_socket = base_url.substr(base_url.find(":") + 1);
                base_url = "http://localhost";
            }
            std::string url = base_url + "/info/current/" + url_path;

            ensure_initialized();

            curl_easy_reset(handle);

            static const char *cookies = getenv("TDAQ_WEBDAQ_COOKIEFILE");
            if(cookies) {
                curl_easy_setopt(handle, CURLOPT_COOKIEFILE, cookies);
            }

            curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);
            if(!unix_socket.empty()) {
                curl_easy_setopt(handle, CURLOPT_UNIX_SOCKET_PATH, unix_socket.c_str());
            }

            static const char *verify = getenv("TDAQ_WEBDAQ_VERIFY");
            static bool add_noverify = verify && strcmp(verify, "0") == 0;
            if(add_noverify) {
                curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 0L);
                curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 0L);
            }

            static const char *verbose = getenv("TDAQ_WEBDAQ_VERBOSE");
            static bool add_verbose = verbose && strcmp(verbose, "0") != 0;
            if(add_verbose) {
                curl_easy_setopt(handle, CURLOPT_VERBOSE, 1L);
            }

            static const char *proxy_negotiate = getenv("TDAQ_WEBDAQ_PROXY_NEGOTIATE");
            static bool add_proxy_negotiate = proxy_negotiate && strcmp(proxy_negotiate, "0") != 0;
            if (add_proxy_negotiate) {
                curl_easy_setopt(handle, CURLOPT_PROXYAUTH, CURLAUTH_NEGOTIATE);
                curl_easy_setopt(handle, CURLOPT_PROXYUSERPWD, ":");
            }

            static const char *proxy = getenv("TDAQ_WEBDAQ_PROXY");
            if(proxy) {
                curl_easy_setopt(handle, CURLOPT_PROXY, proxy);
            }

            return true;
        }

        // Get data as a string
        bool
        curl_get(CURL *handle,
                 const std::string& url_path,
                 std::string& response)
        {
            if(!curl_common(handle, url_path, response)) {
                return false;
            }

            CURLcode res;
            res = curl_easy_perform(handle);

            if(res != CURLE_OK) {
                return false;
            }

            long response_code;
            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if(response_code != 200) {
                return false;
            }

            return true;
        }

        // Get data as a JSON value
        bool
        curl_get(CURL *handle, const std::string& url_path, nlohmann::json& result)
        {
            std::string response;
            if(!curl_get(handle, url_path, response)) {
                return false;
            }

            try {
                result = nlohmann::json::parse(response);
                return true;
            } catch(...) {
                return false;
            }

            return false;
        }

        // Get binary data (raw event data)
        bool
        curl_get(CURL *handle, const std::string& url_path, std::unique_ptr<uint32_t[]>& data, size_t& length)
        {
            std::string dummy;
            if(!curl_common(handle, url_path, dummy)) {
                return false;
            }

            binary_data transfer;
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_binary);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &transfer);
            curl_easy_setopt(handle, CURLOPT_HEADERFUNCTION, header_callback);
            curl_easy_setopt(handle, CURLOPT_HEADERDATA, &transfer);

            CURLcode res;
            res = curl_easy_perform(handle);

            if(res != CURLE_OK) {
                return false;
            }

            long response_code;
            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if(response_code != 200) {
                return false;
            }

            data = std::move(transfer.data);
            length = (transfer.length - 1)/sizeof(uint32_t) + 1;

            return true;
        }

        bool
        curl_delete(CURL *handle, const std::string& url_path)
        {
            std::string response;
            if(!curl_common(handle, url_path, response)) {
                return false;
            }

            curl_easy_setopt(handle, CURLOPT_CUSTOMREQUEST, "DELETE");
            CURLcode res;
            res = curl_easy_perform(handle);

            if(res != CURLE_OK) {
                return false;
            }

            long response_code;
            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if(response_code != 200) {
                return false;
            }

            return true;
        }

        bool
        curl_post(CURL *handle,
                  const std::string& url_path,
                  const std::string& request,
                  std::string& response)
        {
            if(!curl_common(handle, url_path, response)) {
                return false;
            }

            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, request.c_str());

            struct curl_slist *list = NULL;
            list = curl_slist_append(list, "Content-type: application/json");
            curl_easy_setopt(handle, CURLOPT_HTTPHEADER, list);

            CURLcode res;
            res = curl_easy_perform(handle);

            curl_slist_free_all(list); /* free the list */

            if(res != CURLE_OK) {
                return false;
            }

            long response_code;
            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if(response_code != 201) {
                return false;
            }

            return true;
        }

        bool curl_post(CURL *handle, const std::string& url_path, void *data, size_t length, std::string& response)
        {
            if(!curl_common(handle, url_path, response)) {
                return false;
            }

            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, data);
            curl_easy_setopt(handle, CURLOPT_POSTFIELDSIZE, length);

            struct curl_slist *list = NULL;
            list = curl_slist_append(list, "Content-type: application/octetstream");
            curl_easy_setopt(handle, CURLOPT_HTTPHEADER, list);

            CURLcode res;
            res = curl_easy_perform(handle);

            curl_slist_free_all(list); /* free the list */

            if(res != CURLE_OK) {
                return false;
            }

            long response_code;
            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if(response_code != 201) {
                return false;
            }

            return true;
        }

        bool
        curl_post(CURL *handle,
                  const std::string& url_path,
                  const nlohmann::json& data,
                  std::string& response)
        {
            return curl_post(handle, url_path, data.dump(), response);
        }

        bool
        curl_post(CURL *handle,
                  const std::string& url_path,
                  const nlohmann::json& data,
                  nlohmann::json& response)
        {
            std::string result;
            if(!curl_post(handle, url_path, data.dump(), result))
                return false;
            response = nlohmann::json::parse(result);
            return true;
        }

    }
}
