#ifndef WEBDAQ_COMMON_H_
#define WEBDAQ_COMMON_H_

#include "nlohmann/json_fwd.hpp"

#ifndef LIBCURL_VERSION
typedef void CURL;
#endif

namespace webdaq {

    namespace detail {

        // Ensure global curl initialization
        void ensure_initialized();

        // Get thread local global handle, create it if necessary
        CURL *get_handle();

        // Common setup for all curl_get() routines
        bool curl_common(CURL *handle, const std::string &url_path, std::string &response);

        // Get data as a string
        bool curl_get(CURL *handle, const std::string& url_path, std::string& response);

        // Get data as a JSON value
        bool curl_get(CURL *handle, const std::string& url_path, nlohmann::json& result);

        // Get binary data (raw event data)
        bool curl_get(CURL *handle, const std::string& url_path, std::unique_ptr<uint32_t[]>& data, size_t& length);

        bool curl_delete(CURL *handle, const std::string& url_path);

        bool curl_post(CURL *handle, const std::string& url_path, const std::string& request, std::string& response);

        bool curl_post(CURL *handle, const std::string& url_path, void *data, size_t length, std::string& response);

        bool curl_post(CURL *handle, const std::string& url_path, const nlohmann::json& data, std::string& response);

        bool curl_post(CURL *handle, const std::string& url_path, const nlohmann::json& data, nlohmann::json& response);
    }
}


#endif // WEBDAQ_COMMON_H_
