
#include "common.hpp"
#include "webdaq/webdaq.hpp"
#include "webdaq/webdaq-curl.hpp"
#include <nlohmann/json.hpp>

#include <algorithm>
#include <curl/curl.h>
#include <cstdlib>
#include <memory>
#include <string>
#include <vector>

namespace webdaq {

    using namespace webdaq::detail;

    namespace ipc {

        bool partitions(nlohmann::json &results) {
            return partitions(get_handle(), results);
        }

        bool partitions(std::vector<std::string> &results) {
            return partitions(get_handle(), results);
        }
    }

    namespace is {

        bool get(const std::string &partition,
                 const std::string &server,
                 const std::string &name,
                 nlohmann::json &result,
                 bool meta,
                 int tag)
        {
            return get(get_handle(), partition, server, name, result, meta, tag);
        }

        bool get(const std::string &partition,
                 const std::string &server_dot_name,
                 nlohmann::json &result,
                 bool meta,
                 int tag)
        {
            return get(get_handle(), partition, server_dot_name, result, meta, tag);
        }

        bool put(const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 const std::string& type,
                 const nlohmann::json& data,
                 int tag)
        {
            return put(get_handle(), partition, server, name, type, data, tag);
        }

        bool put(const std::string &partition,
                 const std::string &server_dot_name,
                 const std::string &type,
                 const nlohmann::json &data,
                 int tag)
        {
            return put(get_handle(), partition, server_dot_name, type, data, tag);
        }

        bool list(const std::string &partition,
                  const std::string &server,
                  nlohmann::json &results,
                  const std::string &regex,
                  const std::string &type)
        {
            return list(get_handle(), partition, server, results, regex, type);
        }

        bool remove(const std::string &partition,
                    const std::string &server,
                    const std::string &name,
                    int tag)
        {
            return remove(get_handle(), partition, server, name, tag);
        }

        bool remove(const std::string &partition,
                    const std::string &server_dot_name,
                    int tag)
        {
            return remove(get_handle(), partition, server_dot_name, tag);
        }

        bool tags(const std::string &partition,
                  const std::string &server,
                  const std::string &name,
                  std::vector<std::pair<int, int>> &tags_)
        {
            return tags(get_handle(), partition, server, name, tags_);
        }

        bool tags(const std::string &partition,
                  const std::string &server_dot_name,
                  std::vector<std::pair<int, int>> &result)
        {
            return tags(get_handle(), partition, server_dot_name, result);
        }

    }

    namespace oh {

        bool get(const std::string &partition,
                 const std::string &server,
                 const std::string &name,
                 std::string &result,
                 int tag)
        {
            return get(get_handle(), partition, server, name, result, tag);
        }

        bool get(const std::string &partition,
                 const std::string &server_dot_name,
                 std::string &result,
                 int tag)
        {
            return get(get_handle(), partition, server_dot_name, result, tag);
        }

        bool put(const std::string &partition,
                 const std::string &server,
                 const std::string &name,
                 const std::string &histo,
                 int tag)
        {
            return put(get_handle(), partition, server, name, histo, tag);
        }

        bool put(const std::string &partition,
                 const std::string &server_dot_name,
                 const std::string &histo,
                 int tag)
        {
            return put(get_handle(), partition, server_dot_name, histo, tag);
        }

        bool get(const std::string &partition,
                 const std::string &server,
                 const std::string &name,
                 nlohmann::json &histo,
                 int tag)
        {
            return get(get_handle(), partition, server, name, histo, tag);
        }

        bool get(const std::string &partition,
                 const std::string &server_dot_name,
                 nlohmann::json &histo,
                 int tag)
        {
            return get(get_handle(), partition, server_dot_name, histo, tag);
        }

        bool put(const std::string &partition,
                 const std::string &server,
                 const std::string &name,
                 const nlohmann::json &histo,
                 int tag)
        {
            return put(get_handle(), partition, server, name, histo, tag);
        }

        bool put(const std::string &partition,
                 const std::string &server_dot_name,
                 const nlohmann::json &histo,
                 int tag)
        {
            return put(get_handle(), partition, server_dot_name, histo, tag);
        }
    }

    namespace ers {

        bool send(const std::string &partition, const nlohmann::json &issue)
        {
            return send(get_handle(), partition, issue);
        }
    }


    namespace oks {

        bool get(const std::string &partition,
                 const std::string &class_name,
                 const std::string &name,
                 nlohmann::json &result) {
            return get(get_handle(), partition, class_name, name, result);
        }

        bool list(const std::string &partition,
                  const std::string &class_name,
                  nlohmann::json &results)
        {
            return list(get_handle(), partition, class_name, results);
        }
    }

    namespace emon {

        /// Subscribe, success if result not empty
        std::string subscribe(const std::string &partition,
                              const nlohmann::json &subscription)
        {
            return subscribe(get_handle(), partition, subscription);
        }

        /// Unsubscribe
        bool unsubscribe(const std::string &partition,
                         const std::string &subscription_id)
        {
            return unsubscribe(get_handle(), partition, subscription_id);
        }

        /// Get next event
        bool nextEvent(const std::string &partition,
                       const std::string &subscription_id,
                       std::unique_ptr<uint32_t[]> &event,
                       size_t &length,
                       const std::chrono::system_clock::duration &timeout)
        {
            return nextEvent(get_handle(), partition, subscription_id, event, length, timeout);
        }
    }

}
