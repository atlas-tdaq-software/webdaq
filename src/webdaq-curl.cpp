
#include "webdaq/webdaq-curl.hpp"
#include "common.hpp"
#include <nlohmann/json.hpp>

#include <algorithm>
#include <curl/curl.h>
#include <cstdlib>
#include <memory>
#include <string>
#include <vector>
#include <mutex>

namespace webdaq {

    using namespace webdaq::detail;

    std::mutex  base_mutex;
    std::string webdaq_base;

    std::string_view
    base_url()
    {
        if(webdaq_base.empty()) {
            std::lock_guard lock(base_mutex);
            if(webdaq_base.empty()) {
                if(const char *b = std::getenv("TDAQ_WEBDAQ_BASE")) {
                    webdaq_base = b;
                }
            }
        }
        return webdaq_base;
    }

    void base_url(std::string_view base)
    {
        webdaq_base = base;
    }

    namespace ipc {

        bool partitions(CURL *handle, nlohmann::json& results)
        {
            std::string url = "?format=json";
            return curl_get(handle, url, results);
        }

        bool partitions(CURL *handle, std::vector<std::string>& results)
        {
            nlohmann::json res;
            if(partitions(handle, res)) {
                try {
                    results = res.get<std::vector<std::string>>();
                    return true;
                } catch (...) {
                    return false;
                }
            }
            return false;
        }
    }

    namespace is {

        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 nlohmann::json& result,
                 bool meta,
                 int tag)
        {
            std::string url = partition + "/is/" + server + '/' + server + '.' + name + "?format=compact";
            if(tag != -1) {
                url += "&?tag=" + std::to_string(tag);
            }

            if(!curl_get(handle, url, result))
                return false;

            try {
                if(!meta) {
                    result = result[3];
                }
                return true;
            } catch(...) {
                return false;
            }

            return false;
        }

        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& server_dot_name,
                 nlohmann::json& result,
                 bool meta,
                 int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return get(handle,
                           partition,
                           server_dot_name.substr(0, dot),
                           server_dot_name.substr(dot + 1),
                           result,
                           meta,
                           tag);
            }
            return false;
        }

        struct multi_request {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handle{curl_easy_init(), curl_easy_cleanup};
            std::string response;
        };

        bool
        get(const std::string& partition,
            const std::vector<std::string>& server_dot_names,
            std::vector<nlohmann::json>& results,
            bool meta)
        {
            ensure_initialized();

            std::vector<multi_request> requests{server_dot_names.size()};
            std::unique_ptr<CURLM, decltype(&curl_multi_cleanup)> handle{curl_multi_init(), curl_multi_cleanup};
            for(size_t i = 0; auto& req : requests) {
                std::string url_path = partition + "/is/" + server_dot_names[i].substr(0, server_dot_names[i].find('.')) + "/" + server_dot_names[i] + "?format=compact";
                if(!curl_common(req.handle.get(), url_path, req.response)) {
                    return false;
                }
                curl_easy_setopt(req.handle.get(), CURLOPT_PIPEWAIT, 1L);
                curl_multi_add_handle(handle.get(), req.handle.get());
                i++;
            }
            curl_multi_setopt(handle.get(), CURLMOPT_PIPELINING, CURLPIPE_MULTIPLEX);

            int still_running = 0;
            CURLMcode mc;
            do {
                mc = curl_multi_perform(handle.get(), &still_running);

                if(still_running)
                    /* wait for activity, timeout or "nothing" */
                    mc = curl_multi_poll(handle.get(), NULL, 0, 10000, NULL);

                if(mc)
                    break;
            } while(still_running);

            results.clear();

            for(auto& req : requests) {
                curl_multi_remove_handle(handle.get(), req.handle.get());
                if(!mc) {
                    try {
                        auto res = nlohmann::json::parse(req.response);
                        if(!meta) {
                            results.push_back(res[3]);
                        } else {
                            results.push_back(res);
                        }
                    } catch(...) {
                        results.push_back(nullptr);
                    }
                }
            }

            return !mc;
        }

        bool put(CURL *handle,
                 const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 const std::string& type,
                 const nlohmann::json& data,
                 int tag)
        {
            std::string url = partition + "/is/" + server + '/' + server + '.' + name + "?type=" + type;
            if(tag != -1) {
                url += "&tag=" + std::to_string(tag);
            }
            std::string response;
            return curl_post(handle, url, data, response);
        }

        bool put(CURL *handle,
                 const std::string& partition,
                 const std::string& server_dot_name,
                 const std::string& type,
                 const nlohmann::json& data,
                 int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return put(handle,
                           partition,
                           server_dot_name.substr(0, dot),
                           server_dot_name.substr(dot + 1),
                           type,
                           data,
                           tag);
            }
            return false;
        }

        bool list(CURL *handle,
                  const std::string& partition,
                  const std::string& server,
                  nlohmann::json& results,
                  const std::string& regex,
                  const std::string& type
                  )
        {
            std::string url = partition + "/is/" + server + "?format=json";
            if(!regex.empty()) {
                url += "&regex=" + regex;
            }
            if(!type.empty()) {
                url += "&type=" + type;
            }

            return curl_get(handle, url, results);
        }

        bool remove(CURL *handle,
                    const std::string& partition,
                    const std::string& server,
                    const std::string& name,
                    int tag)
        {
            std::string url = partition + "/is/" + server + '/' + server + '.' + name;
            if(tag != -1) {
                url += "&?tag=" + std::to_string(tag);
            }

            return curl_delete(handle, url);
        }

        bool remove(CURL *handle,
                    const std::string& partition,
                    const std::string& server_dot_name,
                    int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
              return remove(handle,
                            partition,
                            server_dot_name.substr(0, dot),
                            server_dot_name.substr(dot + 1),
                            tag);
            }
            return false;
        }

        bool tags(CURL *handle,
                  const std::string& partition,
                  const std::string& server,
                  const std::string& name,
                  std::vector<std::pair<int, int>>& tags_)
        {
            std::string url = partition + "/is/" + server + '/' + server + '.' + name + "?format=json&tags=1";
            nlohmann::json result;
            if(curl_get(handle, url, result)) {
                for(auto& tag : result) {
                    tags_.emplace_back(tag[0], tag[1]);
                }
                return true;
            }
            return false;
        }

        bool tags(CURL *handle,
                  const std::string& partition,
                  const std::string& server_dot_name,
                  std::vector<std::pair<int, int>>& result)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return tags(handle, partition,
                            server_dot_name.substr(0, dot),
                            server_dot_name.substr(dot + 1),
                            result);
            }
            return false;
        }

    }

    namespace oh {
        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 std::string& result,
                 int tag)
        {
            std::string url = partition + "/oh/" + server + "." + name + "?type=compact";
            if(tag != -1) {
                url += "&tag=" + std::to_string(tag);
            }
            return curl_get(handle, url, result);
        }

        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& server_dot_name,
                 std::string& result,
                 int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return get(handle,
                           partition,
                           server_dot_name.substr(0, dot),
                           server_dot_name.substr(dot + 1),
                           result,
                           tag);
            }
            return false;
        }

        bool put(CURL *handle,
                 const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 const std::string& histo,
                 int tag)
        {
            std::string url = partition + "/oh/" + server + "." + name;
            if(tag != -1) {
                url += "?tag=" + std::to_string(tag);
            }
            std::string response;
            return curl_post(handle, url, histo, response);
        }

        bool put(CURL *handle,
                 const std::string& partition,
                 const std::string& server_dot_name,
                 const std::string& histo,
                 int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return put(handle,
                           partition,
                           server_dot_name.substr(0, dot),
                           server_dot_name.substr(dot + 1),
                           histo,
                           tag);
            }
            return false;
        }

        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 nlohmann::json& histo,
                 int tag)
        {
            std::string url = partition + "/oh/" + server + "." + name + "?type=compact";
            if(tag != -1) {
                url += "&tag=" + std::to_string(tag);
            }
            return curl_get(handle, url, histo);
        }

        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& server_dot_name,
                 nlohmann::json& histo,
                 int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return get(handle,
                           partition,
                           server_dot_name.substr(0, dot),
                           server_dot_name.substr(dot + 1),
                           histo,
                           tag);
            }
            return false;
        }

        bool put(CURL *handle,
                 const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 const nlohmann::json& histo,
                 int tag)
        {
            std::string url = partition + "/oh/" + server + "." + name;
            if(tag != -1) {
                url += "?tag=" + std::to_string(tag);
            }
            std::string response;
            return curl_post(handle, url, histo, response);
        }

        bool put(CURL *handle,
                 const std::string& partition,
                 const std::string& server_dot_name,
                 const nlohmann::json& histo,
                 int tag)
        {
            auto dot = server_dot_name.find('.');
            if(dot != std::string::npos) {
                return put(handle,
                           partition,
                           server_dot_name.substr(0, dot),
                           server_dot_name.substr(dot + 1),
                           histo,
                           tag);
            }
            return false;
        }

    }

    namespace ers {

        bool send(CURL *handle, const std::string& partition, const nlohmann::json& issue)
        {
            std::string response;
            return curl_post(handle, partition + "/ers", issue, response);
        }

    }

    namespace oks {

        bool get(CURL *handle,
                 const std::string& partition,
                 const std::string& class_name,
                 const std::string& name,
                 nlohmann::json& result)
        {
            return curl_get(handle, partition + "/oks/" + class_name + '/' + name + "?format=compact", result);
        }

        bool list(CURL *handle,
                  const std::string& partition,
                  const std::string& class_name,
                  nlohmann::json& results)
        {
            return curl_get(handle, partition + "/oks/" + class_name + "?format=json", results);
        }

    }

    namespace emon {

        /// Subscribe, success if result not empty
        std::string subscribe(CURL *handle,
                              const std::string& partition,
                              const nlohmann::json& subscription)
        {
            nlohmann::json response;
            if(curl_post(handle, partition + "/event/", subscription, response)) {
                return response["id"];
            }
            return {};
        }

        /// Unsubscribe
        bool unsubscribe(CURL *handle,
                         const std::string& partition,
                         const std::string& subscription_id)
        {
            return curl_delete(handle, partition + "/event/" + subscription_id);
        }

        /// Get next event
        bool nextEvent(CURL *handle,
                       const std::string& partition,
                       const std::string& subscription_id,
                       std::unique_ptr<uint32_t[]>& event,
                       size_t& length,
                       const std::chrono::system_clock::duration& timeout)
        {
            return curl_get(handle, partition + "/event/" + subscription_id, event, length);
        }

    }

}
