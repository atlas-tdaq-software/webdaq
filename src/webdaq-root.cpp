
#include "webdaq/webdaq-root.hpp"
#include "webdaq/webdaq-curl.hpp"
#include "common.hpp"

#include "TBufferFile.h"

namespace webdaq {

    using namespace webdaq::detail;

    namespace oh {

        // 'obj' can be TH*, TGraph*, TEfficiency*

        bool put(const std::string& partition,
                 const std::string& server,
                 const std::string& name,
                 TObject *obj)
        {
            TBufferFile buffer(TBuffer::kWrite);
            buffer.WriteObject(obj);
            std::string response;
            return curl_post(get_handle(), partition + "/oh/" + server + "." + name, buffer.Buffer(), buffer.Length(), response);
        }

        bool put(const std::string& partition,
                 const std::string& server_dot_name,
                 TObject *obj)
        {
            TBufferFile buffer(TBuffer::kWrite);
            buffer.WriteObject(obj);
            std::string response;
            return curl_post(get_handle(), partition + "/oh/" + server_dot_name, buffer.Buffer(), buffer.Length(), response);
        }

    }
}
