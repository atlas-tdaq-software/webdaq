
#include "webdaq/webdaq.hpp"
#include <nlohmann/json.hpp>

#include <iostream>

int main(int argc, char *argv[])
{
    using nlohmann::json;
    using namespace webdaq;

    std::string part{"test"};

    if(argc > 1)
        part = argv[1];

    json partition_list;

    bool have_test_partition{false}, have_initial_partition{false};

    if(ipc::partitions(partition_list)) {
        have_test_partition = std::find(partition_list.begin(), partition_list.end(), part) != partition_list.end();
        have_initial_partition = std::find(partition_list.begin(), partition_list.end(), "initial") != partition_list.end();
    }

    if (!have_initial_partition) {
        std::cerr << "No initial partition\n";
        exit(1);
    }

    json runparams;

    // get object including meta information (name, type, time stamp)
    if(is::get("initial", "RunParams", "RunParams", runparams, true)) {
        std::cout << "Got result with meta data:\n" << runparams << std::endl;
    } else {
        std::cerr << "Retrieval failed\n";
    }

    // only get object attributes
    if(is::get("initial", "RunParams", "RunParams", runparams)) {
        std::cout << "Got result:\n" << runparams << std::endl;
    } else {
        std::cerr << "Retrieval failed\n";
    }

    // Same with combined server.name
    if(is::get("initial", "RunParams.RunParams", runparams)) {
        std::cout << "Got result:\n" << runparams << std::endl;
    } else {
        std::cerr << "Retrieval failed\n";
    }

    // get the tags
    std::vector<std::pair<int,int>> tags;
    if(is::tags("initial", "RunParams", "RunParams", tags)) {
        std::cout << "Tags:\n";
        for(auto& tag : tags) {
            std::cout << "(" << tag.first << ", " << tag.second << ")\n";
        }
    } else {
        std::cerr << "Tag retrieval failed\n";
    }

    // checkin a modified copy
    runparams["run_number"] = 12345;
    if(is::put("initial", "RunParams", "RunParams-new", "RunParams", runparams)) {
        std::cout << "Updated run params\n";
        if(is::get("initial", "RunParams", "RunParams-new", runparams)) {
            std::cout << runparams << std::endl;
        } else {
            std::cerr << "Retrieval of updated failed\n";
        }
    } else {
        std::cerr << "Update did not work\n";
    }

    // delete our version
    if(is::remove("initial", "RunParams", "RunParams-new")) {
        std::cout << "Deleting object successful !" << std::endl;
    } else {
        std::cerr << "Deleting object failed !\n";
    }

    // list IS objects

    json results;
    if(is::list("initial", "RunCtrl", results)) {
        std::cout << "List RunCtrl:\n" << results << std::endl;
    } else {
        std::cerr << "Cannot list IS server\n";
    }

    if(is::list("initial", "RunCtrl", results, "DefaultRoot.*")) {
        std::cout << "List RunCtrl with regular expression:\n" << results << std::endl;
    } else {
        std::cerr << "Cannot list IS server with expression \n";
    }

    if(is::list("initial", "RunCtrl", results, "", "RCStateInfo")) {
        std::cout << "List RunCtrl with type:\n" << results << std::endl;
    } else {
        std::cerr << "Cannot list IS server with type\n";
    }

    // no histogram in 'initial', so assume 'test' partition exists
    // get histogram

    if(have_test_partition) {
        json histo;
        if(oh::get(part, "Histogramming", "HLTSV.ProcessingTime", histo)) {
            std::cout << "Histogram:\n" << histo << std::endl;
        } else {
            std::cerr << "Cannot retrieve histogram\n";
        }

        // publish histogram
        if(oh::put(part, "Histogramming", "WEBDAQ.ProcessingTime", histo)) {
            std::cout << "Histogram published !" << std::endl;
            if(!oh::put(part, "Histogramming", "WEBDAQ.ProcessingTime", histo)) {
                std::cerr << "Second publication failed\n";
            }
        } else {
            std::cerr << "Histogram publishing failed !\n";
        }

        // delete histogram
        if(is::remove(part, "Histogramming", "WEBDAQ.ProcessingTime")) {
            std::cout << "Deleting histogram successful" << std::endl;
        } else {
            std::cerr << "Histogram deletion failed !\n";
        }
    } else {
        std::cerr << "WARNING: histogram tests were skipped because partition does not exits: " << part << std::endl;
    }

    // send an ERS issue
    json issue = {
        { "sev" , "ERROR" },
        { "func", "test_webdaq()" },
        { "line", __LINE__ },
        { "msg", "MyIssue" },
        { "message", "This is the error message" }
    };

    if(ers::send("initial", issue)) {
        std::cout << "Sent ERS issue !" << std::endl;
    } else {
        std::cerr << "Sending ERS issue failed !\n";
    }

    // list OKS objects
    json partitions, initial;
    if(oks::list("initial", "Partition", partitions)) {
        std::cout << "Partitions:\n " << partitions << std::endl;
    } else {
        std::cerr << "Could not list OKS partitions\n";
    }

    // get an OKS object
    if(oks::get("initial", "Partition", "initial", initial)) {
        std::cout << "initial partition:\n " << initial << std::endl;
    } else {
        std::cerr << "Could not retrieve OKS object for initial partition\n";
    }

    if(have_test_partition) {
        // subscribe to test partition
        // This assumes a partition generated with pm_part_hlt
        json event_subscription{
            { "sampler_type", "dcm" },
            { "stream_logic", "OR" },
            { "stream_tag", "physics" },
            { "stream_names", { "L2-CaloPhysics-Tag-0" } }
        };

        auto sub = emon::subscribe(part, event_subscription);
        std::unique_ptr<uint32_t[]> event;
        size_t length;
        if(emon::nextEvent(part, sub, event, length)) {
            std::cout << "Got an event ! Size = " << length << " words" << std::endl;
            for(size_t i = 0; i < length; i++) {
                if(i > 10) break;
                std::cout << std::setw(8) << std::setfill('0') << std::hex << event[i] << " ";
            }
            std::cout << std::endl;
        } else {
            std::cerr << "Did not get an event !\n";
        }

        if(emon::unsubscribe(part, sub)) {
            std::cout << "Unsubscribed from " << sub << std::endl;
        } else {
            std::cerr << "Could not unsubscribe from " << sub << std::endl;
        }
    } else {
        std::cerr << "WARNING: emon tests were skipped because partition does not exist: " << part << std::endl;
    }

    return 0;
}
