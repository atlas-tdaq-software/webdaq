
#include "webdaq/webdaq.hpp"
#include <iostream>
#include <nlohmann/json.hpp>

#include <string>
#include <vector>

int main(int argc, char *argv[])
{
    argc--;
    argv++;

    bool meta = false;
    if(argc > 0 && strcmp(argv[0], "-m") == 0) {
        meta = true;
        argc--;
        argv++;
    }

    if(argc < 2) {
        std::cerr << "Usage: test-parallel <partition> [ server.name ...]" << std::endl;
        return 1;
    }

    std::string partition{argv[0]};
    std::vector<std::string> requests;
    std::vector<nlohmann::json> results;

    for(int i = 1; i < argc; i++) {
        requests.push_back(argv[i]);
    }

    if(webdaq::is::get(partition, requests, results, meta)) {
        for(auto& res : results) {
            std::cout << res << "\n\n";
        }
    }
}
