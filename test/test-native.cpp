
// test-native [partition [count [threads [instance]]]]

#include "webdaq/webdaq-root.hpp"
#include "TH1F.h"
#include <TROOT.h>

#include <iostream>
#include <chrono>
#include <ostream>
#include <thread>
#include <vector>
#include <mutex>

void worker(int idx, int count, const std::string& instance)
{
    TH1F *h = new TH1F((std::string("test") + std::to_string(idx)).c_str(),"test", 100, 0., 100.);
    for(int i = 0; i < 100; i++) h->Fill(i, i);

    std::string p("WEBDAQ-ROOT");
    p += instance + "-";
    p += std::to_string(idx) + ".test-";

    for(int i = 0; i < count; i++) {
        if(!webdaq::oh::put("test", "Histogramming", p + std::to_string(i) /* + "/DEBUG/Lar/test" */, h)) {
           std::cerr << "Error at " << idx << ", " << i << std::endl;
        }
    }

    delete h;
}

int main(int argc, char *argv[])
{
    std::string partition{"test"};
    int count = 1000;
    int threads = 1;
    std::string instance = "0";

    if(argc > 1) {
        partition = argv[1];
        argc--;
        argv++;
    }

    if(argc > 1) {
        count = std::stoi(argv[1]);
        argc--;
        argv++;
    }

    if(argc > 1) {
        threads = std::stoi(argv[1]);
        argc--;
        argv++;
    }

    if(argc > 1) {
        instance = argv[1];
    }

    ROOT::EnableThreadSafety();

    std::vector<std::thread> threadlist;
    auto start = std::chrono::system_clock::now();
    for(int i = 0; i < threads; i++) {
        threadlist.emplace_back(worker, i, count, instance);
    }

    for(auto& t : threadlist) {
        t.join();
    }

    auto diff = std::chrono::system_clock::now() - start;
    std::cout << "Total time = " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count()  << " milliseconds" << std::endl
              << "Time/histo = " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count()/(count * threads)  << " microseconds" << std::endl;

}
