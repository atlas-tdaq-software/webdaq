
#include "webdaq/webdaq.hpp"
#include <nlohmann/json.hpp>

#include <TH1F.h>
#include <TBufferJSON.h>
#include <TROOT.h>

#include <iostream>
#include <chrono>
#include <ostream>
#include <thread>
#include <vector>
#include <mutex>

std::mutex m;

void worker(int num, TH1F *h, const std::string& partition, int count)
{
    using namespace webdaq;

    for(int i = 0; i < count; i++) {
        TString histo;
        {
            // ugh, not thread safe, aborts in cling when run with more than one thread
            // std::scoped_lock lock(m);
            histo = TBufferJSON::ConvertToJSON(h);
        }
        if(!oh::put(partition, "Histogramming", "WEBDAQ.ProcessingTime" + std::to_string(num) + "-" + std::to_string(i), histo)) {
            std::cerr << "Histogram publishing failed !\n";
        }
    }
}

int main(int argc, char *argv[])
{
    using nlohmann::json;
    using namespace webdaq;

    std::string partition{"test"};
    int count = 10000;
    int threads = 10;

    if(argc > 1) {
        partition = argv[1];
        argc--;
        argv++;
    }

    if(argc > 1) {
        count = std::stoi(argv[1]);
        argc--;
        argv++;
    }

    if(argc > 1) {
        threads = std::stoi(argv[1]);
    }

    ROOT::EnableThreadSafety();

    // get histogram
    std::string histo;
    if(!oh::get(partition, "Histogramming", "HLTSV.ProcessingTime", histo)) {
        std::cerr << "Cannot retrieve histogram\n";
        exit(1);
    }

    TH1F *h = dynamic_cast<TH1F*>(TBufferJSON::ConvertFromJSON(histo.c_str()));

    auto start = std::chrono::system_clock::now();
    std::vector<std::thread> workers;
    for(int i = 0; i < threads; i++) {
        workers.emplace_back(worker, i, h, partition, count);
    }

    for(int i = 0; i < threads; i++) {
        workers[i].join();
    }

    auto diff = std::chrono::system_clock::now() - start;
    std::cout << "Total time = " << std::chrono::duration_cast<std::chrono::milliseconds>(diff).count()  << " milliseconds" << std::endl
              << "Total count = " << threads * count << std::endl
              << "Time/histo = " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count()/(threads * count)  << " microseconds" << std::endl;

}

